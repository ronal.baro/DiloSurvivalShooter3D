﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScCameraFollow : MonoBehaviour
{
    public Transform _target;
    public float _smoothing = 5f;

    Vector3 _offset;
    internal bool _shake = false;

    float _time=0f;
    public float _shakeDuration = 0.2f;

    void Start()
    {
        _offset = transform.position - _target.position;
    }
    void FixedUpdate()
    {
        Vector3 targetCamPos = _target.position + _offset; //jaga jarak

        transform.position = Vector3.Lerp(transform.position, targetCamPos, _smoothing * Time.deltaTime);
        if (_shake)
        {
            Shake();
        }

    }
    void Shake()
    {
        if (_time >= _shakeDuration)
        {
            _shake = false;
            _time=0f;
            return;
        }
        transform.position += (Random.insideUnitSphere * 0.5f);
        _time += Time.deltaTime;

    }
}
