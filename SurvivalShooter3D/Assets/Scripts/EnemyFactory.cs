﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFactory : MonoBehaviour, IFactory
{
    [SerializeField]
    GameObject[] _enemyPrefab;
    public GameObject ProduceEnemy(int tag)
    {
        GameObject enemy = Instantiate(_enemyPrefab[tag]);
        return enemy;
    }

}
