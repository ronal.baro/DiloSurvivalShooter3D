﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScFirstAid : MonoBehaviour
{
    public int _nilaiGizi = 20;
    private ScPlayerHealth _playerHealth;
    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Player"){
            _playerHealth = other.gameObject.GetComponent<ScPlayerHealth>();
            _playerHealth.Heal(_nilaiGizi);
            Destroy(this.gameObject.transform.parent.gameObject);

        }
    }
}
