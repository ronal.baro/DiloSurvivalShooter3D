﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootCommand : Command
{
    ScPlayerShooting _playerShoot;
    
    public ShootCommand(ScPlayerShooting playerShoot){
        this._playerShoot = playerShoot;
    }

    public override void Execute()
    {
        _playerShoot.Shoot(0);
    }

    public override void UnExecute()
    {
        
    }
}
