﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public ScPlayerMovement _playerMove;
    public ScPlayerShooting _playerShoot;
    Queue<Command> _commands = new Queue<Command>();

    Command moveCommand;
    Command shootCommand;
    void Update()
    {
        moveCommand = InputMovementHandling();
        shootCommand = InputShootHandling();
        if (shootCommand != null)
        {
            _commands.Enqueue(shootCommand);
            shootCommand.Execute();
        }
    }

    private void FixedUpdate()
    {
        if (moveCommand != null)
        {
            _commands.Enqueue(moveCommand);
            moveCommand.Execute();
        }
    }

    Command InputMovementHandling()
    {
        if (Input.GetKey(KeyCode.D))
        {
            return new MoveCommand(_playerMove, 1, 0);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            return new MoveCommand(_playerMove, -1, 0);
        }
        else if (Input.GetKey(KeyCode.W))
        {
            return new MoveCommand(_playerMove, 0, 1);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            return new MoveCommand(_playerMove, 0, -1);
        }
        else if (Input.GetKey(KeyCode.Z))
        {
            return Undo();
        }
        else
            return null; //soalnya bakal tetatp enqueue kalau gak nul
    }

    Command Undo()
    {
        if (_commands.Count > 0)
        {
            Command undoCommand = _commands.Dequeue();//baru tau kalau dia return queue terakhir
            undoCommand.UnExecute();
        }
        return null;
    }

    Command InputShootHandling()
    {
        //Jika menembak trigger shoot command
        if (Input.GetButtonDown("Fire1"))
        {
            return new ShootCommand(_playerShoot);
        }
        else
        {
            return null;
        }
    }
}
