﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCommand : Command
{
    ScPlayerMovement _playerMove;
    float _h, _v;

    public MoveCommand(ScPlayerMovement playerMove, float h , float v ){
        this._playerMove = playerMove;
        this._h = h;
        this._v = v;
    }

    public override void Execute(){
        _playerMove.Move(_h,_v);
        _playerMove.Animating(_h,_v);
    }

    public override void UnExecute()
    {
        _playerMove.Move (-_h,-_v);
        _playerMove.Animating (-_h,-_v);
    }
}
