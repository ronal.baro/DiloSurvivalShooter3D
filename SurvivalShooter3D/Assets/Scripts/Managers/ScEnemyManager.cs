﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScEnemyManager : MonoBehaviour
{
    public ScPlayerHealth _playerHealth;
    public GameObject _enemy;
    public float _spawnTime = 3.5f;

    public Transform[] _spawnPoints;

    void Start()
    {
        InvokeRepeating("Spawn", _spawnTime, _spawnTime);
    }

    // Update is called once per frame
    void Spawn()
    {
        //
        if(_playerHealth.CurrentHealth <=0f){
            CancelInvoke();
            return;
        }

        int spawnPointIndex = Random.Range(0,_spawnPoints.Length);

        Instantiate (_enemy, _spawnPoints[spawnPointIndex].position, _spawnPoints[spawnPointIndex].rotation);
        
    }
}
