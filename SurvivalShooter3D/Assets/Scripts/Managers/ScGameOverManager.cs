﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class ScGameOverManager : MonoBehaviour
{
     public UnityEngine.UI.Text warningText;
    public ScPlayerHealth _playerHealth;
    private Animator _anim;
    public float _restartDelay = 5f;
    
    float _restartTimer;   

     private void Awake()
    {
        _anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        if (_playerHealth.CurrentHealth <= 0)
        {
            _anim.SetTrigger("GameOver");

            _restartTimer += Time.deltaTime;

            if (_restartTimer >= _restartDelay)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }

    public void ShowWarning(float enemyDistance){
        warningText.text =string.Format("! {0} m", Mathf.RoundToInt(enemyDistance));
        _anim.SetTrigger("Warning");
    }
}
