﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScEnemyManager2 : MonoBehaviour
{
    public ScPlayerHealth _playerHealth;
    public float _spawnTime = 3.5f;

    [SerializeField]
    MonoBehaviour factory;
    IFactory Factory{get{return factory as IFactory;}}

    List<GameObject> _enemies = new List<GameObject>();
    
    //[SerializeField] EnemyFactory Factory; //kedepannya pake ini aja aku

    void Start()
    {
        InvokeRepeating("Spawn", _spawnTime, _spawnTime);
        
    }

    // Update is called once per frame
    void Spawn()
    {
        //
        if(_playerHealth.CurrentHealth <=0f){
            CancelInvoke();
            return;
        }

        int SpawnEnemyIndex = Random.Range(0,3);

        _enemies.Add(Factory.ProduceEnemy(SpawnEnemyIndex));  
    }
}
