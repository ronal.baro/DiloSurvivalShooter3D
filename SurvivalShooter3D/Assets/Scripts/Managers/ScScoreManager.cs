using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScScoreManager : MonoBehaviour
{
    // Start is called before the first frame update

    public UnityEngine.UI.Text text;
    private static int score;
    public static void Score(int val)
    {
        score += val;
    }


    void Awake()
    {
        score = 0;
    }


    void Update()
    {
        text.text = "Score: " + score;
    }

    
}
