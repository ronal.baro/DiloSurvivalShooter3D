using UnityEngine;

public class ScPlayerMovement : MonoBehaviour
{
    public float _speed = 6f;
    Vector3 _movement;
    Animator _anim;
    internal Rigidbody _rbPlayer;
    int _floorMask;

    //Move
    float _inputHor;
    float _inputVer;

    //Animating
    bool _isWalking = false;
    
    //Turning
    Ray _rayOrigin;
    RaycastHit _floorHit;
    
    float camRayLength = 100f; 

    private void Awake()
    {
        _floorMask = LayerMask.GetMask("Floor");

        _anim = GetComponent<Animator>();

        _rbPlayer = GetComponent<Rigidbody>();
    }

    private void Update()
    {   //  aku buat variable global karna ku pikir deklarasi variable berulang2 setiap frame bukanlah langkah yg bijak
         _inputHor = Input.GetAxisRaw("Horizontal");
         _inputVer = Input.GetAxisRaw("Vertical");
    }

    private void FixedUpdate()
    {
       Move(_inputHor,_inputVer);
        Turning();
        Animating(_inputHor,_inputVer);

    }

    public void Move(float h, float v)
    {
        _movement.Set(h, 0f, v);
        _movement = _movement.normalized * _speed  * Time.fixedDeltaTime;

        _rbPlayer.MovePosition(transform.position + _movement);

    }
    void Turning(){
        _rayOrigin = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(Physics.Raycast(_rayOrigin, out _floorHit,camRayLength,_floorMask)){
            Vector3 playerToMouse = _floorHit.point - transform.position;
            playerToMouse.y = 0f;

            _rbPlayer.MoveRotation(Quaternion.LookRotation(playerToMouse));

        }
        
    }

    public void Animating(float h, float v)
    {   //cek apakah berubah.. kalau berubah ya animasinya berubah..kalau ga ya gak berubah
        // idk apakah performa lebih cepat atau lambat.. rasanya aneh aja animasinya diset berulang2 walaupun kondisinya sama..

        if ((h != 0 || v != 0) != _isWalking) 
        {   
            _isWalking = !_isWalking;
            _anim.SetBool("IsWalking", _isWalking);
        }

        //_anim.SetBool("IsWalking", _inputHor != 0 || _inputVer != 0);
    
    
    }
}
