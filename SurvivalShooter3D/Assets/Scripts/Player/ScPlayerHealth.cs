﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScPlayerHealth : MonoBehaviour
{
    private int _startingHealth = 100;
    private int _currentHealth;
    public int CurrentHealth
    {
        get { return _currentHealth; }
    }

    public Slider _healthSlider;
    public Image _damageImage;

    public AudioClip _deathClip;
    public AudioClip _pickUpClip1;
    public AudioClip _fullClip;
    public AudioClip _speedClip;


    public float _flashSpeed = 5f;

    public Color _flashColor = new Color(1f, 0f, 0f, 0.1f);


    Animator _anim;

    AudioSource _playerAudio;

    ScPlayerMovement _move;

    public ScCameraFollow _camera;
    ScPlayerShooting _shoot;

    bool _isDead = false;
    bool _damaged = false;
    void Awake()
    {
        _anim = GetComponent<Animator>();
        _playerAudio = GetComponent<AudioSource>();
        _move = GetComponent<ScPlayerMovement>();
        _shoot = GetComponentInChildren<ScPlayerShooting>();

        _currentHealth = _startingHealth;
    }

    void Update()
    {
        if (_damaged)
        {
            _damageImage.color = _flashColor;

            _damaged = false;
        }
        else
        {

            _damageImage.color = Color.Lerp(_damageImage.color, Color.clear, _flashSpeed * Time.deltaTime);
        }

    }

    public void TakeDamage(int amount)
    {
        _damaged = true;
        _currentHealth -= amount;
        _camera._shake = true;
        _healthSlider.value = _currentHealth;
        _playerAudio.Play();

        float f = amount;
        _shoot._attackRate -= f / 2000f;

        Debug.Log(_shoot._attackRate);
        if (_currentHealth <= 0 && !_isDead)
        {

            Death();
        }
    }

    public void Heal(int amount)
    {
        if (_currentHealth == _startingHealth)
        {
            _playerAudio.PlayOneShot(_fullClip);
            return;
        }

        int tempHealth=_currentHealth + amount;
        if ( tempHealth >  _startingHealth)
        {
            amount -= tempHealth - _startingHealth;//dikurangi lebih..
            _currentHealth = _startingHealth;
        }

        else _currentHealth +=amount;



        _healthSlider.value = _currentHealth;
        _playerAudio.PlayOneShot(_pickUpClip1);

        float f = amount;
        _shoot._attackRate += f / 2000f;
        Debug.Log(_shoot._attackRate);
       

    }

    public void Speed(float amount)
    {
        _playerAudio.PlayOneShot(_speedClip);
        _shoot.PowerUP();
    }

    void Death()
    {
        _isDead = true;
        _shoot.enabled = false;
        _anim.SetBool("IsDead", true);

        _playerAudio.PlayOneShot(_deathClip);
        _move._rbPlayer.isKinematic = true;
        _move.enabled = false;

    }

    public void RestartLevel()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
