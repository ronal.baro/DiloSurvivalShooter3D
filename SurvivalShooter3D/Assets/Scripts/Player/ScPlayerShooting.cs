﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScPlayerShooting : MonoBehaviour
{

    public int _damagePerShot = 20;
    public float _attackRate = 0.15f;
    public float _attackRatePower = 0.07f;

    public float _spreadLmao = 0.05f;
    public float _spreadPower = 0.03f;
    public float _range = 100f;

    bool _powerActive = false;
    float _timer;
    public int _PowerMag = 100;
    public int _magazine;
    Ray _shootRay = new Ray();
    RaycastHit _shoothit;
    int _shootableMask;
    ParticleSystem _gunParticles;
    LineRenderer _gunLine;
    AudioSource _gunAudio;
    Light _gunLight;
    float _effectDisplayTime = 0.2f;
    public ScPlayerMovement _move;
    private TrailRenderer _trail;

    private void Awake()
    {
        _shootableMask = LayerMask.GetMask("Shootable");

        _gunParticles = GetComponent<ParticleSystem>();
        _gunLine = GetComponent<LineRenderer>();
        _gunAudio = GetComponent<AudioSource>();
        _gunLight = GetComponent<Light>();
          _trail = GetComponentInParent<TrailRenderer>();
    }

    private void Update()
    {
        _timer += Time.deltaTime;



        if (!_powerActive && Input.GetButton("Fire1") && _timer >= _attackRate && Time.timeScale != 0)
        {
            Shoot(0);

        }
        else if (_powerActive && _timer >= _attackRatePower)
        {
            _magazine++;

            if (_magazine > _PowerMag)
            {
                _powerActive = false;
                _move._speed = 6f;
                 _trail.enabled = false;
            }

            Shoot(_spreadPower);
        }

        if (_timer >= _attackRate * _effectDisplayTime)
        {
            DisableEffect();
        }
    }

    void DisableEffect()
    {
        _gunLine.enabled = false;
        _gunLight.enabled = false;
    }

    public void Shoot(float spreadPower)
    {

        _timer = 0f;

        _gunAudio.Play();

        _gunLight.enabled = true;
        _gunParticles.Stop();
        _gunParticles.Play();

        _gunLine.enabled = true;
        _gunLine.SetPosition(0, transform.position);

        _shootRay.origin = transform.position;

        _shootRay.direction = transform.forward + Random.insideUnitSphere * (_spreadLmao + spreadPower);

        if (Physics.Raycast(_shootRay, out _shoothit, _range, _shootableMask))
        {
            ScEnemyHealth enemyHealth = _shoothit.collider.GetComponent<ScEnemyHealth>();
            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(_damagePerShot, _shoothit.point);
            }
            _gunLine.SetPosition(1, _shoothit.point);
        }
        else
        {
            _gunLine.SetPosition(1, _shootRay.origin + _shootRay.direction * _range);
        }


    }

    public void PowerUP()
    {
        _magazine = 0;
        _powerActive = true;
        _move._speed = 10f;
        _trail.enabled = true;

    }


}
