﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScIonWater : MonoBehaviour
{
    public float _speedPlus = 20f;
    private ScPlayerHealth _playerHealth;
    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Player"){
            _playerHealth = other.gameObject.GetComponent<ScPlayerHealth>();
            _playerHealth.Speed(_speedPlus);
            Destroy(this.gameObject.transform.parent.gameObject);

        }
    }
}
