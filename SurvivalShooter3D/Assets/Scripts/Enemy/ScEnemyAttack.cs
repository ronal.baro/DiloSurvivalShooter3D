﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScEnemyAttack : MonoBehaviour
{
    public float _attackRate = 0.5f;
    public int _attackDamage = 10;

    Animator _anim;

    GameObject _player;
    ScPlayerHealth _playerHealth;
    ScEnemyHealth _enemyHealth;

    bool _isPlayerInRange;

    float _timer;

    void Awake() {
     _player = GameObject.FindGameObjectWithTag("Player");

     _playerHealth = _player.GetComponent<ScPlayerHealth>();

     _anim = GetComponent<Animator>();
     _enemyHealth = GetComponent<ScEnemyHealth>(); 
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject == _player){
            _isPlayerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.gameObject == _player){
            _isPlayerInRange = false;
        }
    }

    private void Update() {
        _timer += Time.deltaTime;{
            if (_timer >=_attackRate && _isPlayerInRange && _enemyHealth.CurrentHealth>0)
            { 
                Attack();
            }
        }

        if(_playerHealth.CurrentHealth<=0){
            _anim.SetTrigger("PlayerDead");
        }
    }

    void Attack(){
        _timer = 0f;

        if(_playerHealth.CurrentHealth>0){
            _playerHealth.TakeDamage(_attackDamage);
        }
    }
}
