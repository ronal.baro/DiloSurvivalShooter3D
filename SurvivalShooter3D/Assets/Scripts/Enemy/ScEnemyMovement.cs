﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ScEnemyMovement : MonoBehaviour
{
    Transform _player;
    ScPlayerHealth _playerHP;
    ScEnemyHealth _enemyHP;

    NavMeshAgent _navEnemy;
    void Awake()
    {
       _player = GameObject.FindGameObjectWithTag("Player").transform; 
        
        _playerHP = _player.GetComponent<ScPlayerHealth>();
        _enemyHP = GetComponent<ScEnemyHealth>();

        _navEnemy = GetComponent<NavMeshAgent>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(_enemyHP.CurrentHealth>0 && _playerHP.CurrentHealth>0){
            _navEnemy.SetDestination(_player.position);
        }
        else
        {
            _navEnemy.enabled = false;
        }
    }
}
