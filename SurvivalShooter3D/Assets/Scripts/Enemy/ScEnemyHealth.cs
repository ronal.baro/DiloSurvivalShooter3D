﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScEnemyHealth : MonoBehaviour
{
    private int _startingHealth = 100;
    private int _currentHealth;
    public int CurrentHealth
    {
        get { return _currentHealth; }
    }

    public int _priceOnHead = 10;

    public float _sinkSpeed = 2.5f;

    public AudioClip _deathClip;
    public GameObject[] _drop;

    Animator _anim;
    AudioSource _enemyAudio;
    ParticleSystem _hitParticle;
    CapsuleCollider _capsuleCollider;

    bool _isDead;
    bool _isSinking;

    void Awake()
    {
        _anim = GetComponent<Animator>();
        _enemyAudio = GetComponent<AudioSource>();
        _hitParticle = GetComponentInChildren<ParticleSystem>();
        _capsuleCollider = GetComponent<CapsuleCollider>();

        _currentHealth = _startingHealth;
    }

    private void Update()
    {
        if (_isSinking)
        {
            transform.Translate(Vector2.down * _sinkSpeed * Time.deltaTime);
        }

    }

    public void TakeDamage(int amount, Vector3 hitPoint)
    {
        if (_isDead)
        {
            return;
        }

        _enemyAudio.Play();

        _currentHealth -= amount;

        _hitParticle.transform.position = hitPoint;

        _hitParticle.Play();

        if (_currentHealth <= 0)
        {
            Death();
        }


    }
    void Death()
    {
        _isDead = true;
        _capsuleCollider.isTrigger = true;

        _anim.SetTrigger("Dead");

        _enemyAudio.PlayOneShot(_deathClip);

        GetComponent<Rigidbody>().isKinematic = true;
    }

    public void StartSinking()
    {
        GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
        _isSinking = true;
        ScScoreManager.Score(_priceOnHead);
        Destroy(gameObject, 2);
        DropItem();
    }

    void DropItem()
    {
        int whatToDrop = Random.Range(0, 100);
        int i=0;
        if (whatToDrop < 75)
        {   
            return;
        }
        else if (whatToDrop < 90)
        {
            i = 0;
        }
        else if (whatToDrop <= 100)
        {
            i = 1;
        } 
        Instantiate(_drop[i], transform.position, transform.rotation);

    }
}
